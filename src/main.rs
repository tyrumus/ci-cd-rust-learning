pub fn greeting(name: &str) -> String {
    format!("Hello, {}!", name)
}

fn main() {
    println!("{}", greeting("Ronius"));
}

#[cfg(test)]
mod tests {
    #[test]
    fn greeting_test() {
        let result = crate::greeting("Ron");
        assert!(result.contains("Ron"), "Greeting failure. Value was `{}`", result);
    }
}
